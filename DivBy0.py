from lib import (Parkour, Tracker, Thesis)
from cnorm.nodes import (Id, BlockStmt)
from cnorm.passes import (to_c)


def fortrack(self, node, parent, *args, **kwargs):
    """
    Forward tracking function that manage scope. It defines if the scope is
    named or not and push it into a stack.

    * BlockStmt node defines scope embraces in braces
    """
    if isinstance(node, BlockStmt) and parent is not None:
        scope_manager = self.scope(parent)
        scope_manager(parent)

def backtrack(self, node, parent, *args, **kwargs):
    """
    Back tracking function that pop scope when it's closed
    """
    if isinstance(node, BlockStmt) and parent is not None:
        self._scopes.pop()


class DivBy0(Parkour):
    """
    DivBy0 is the object that define the prototype which determine if there is
    db0 in c-files.

    DivBy0 is a Parkour object
    """
    loop_id = 1

    def __init__(self, filename, verbosity, report_file, *args):
        """
        here is all the switchers we need in this prototype
        """
        super(DivBy0, self).__init__(filename, verbosity)
        self._report_file = report_file
        self._thesis = Thesis(self._report_file)
        self._scopes = []
        self._vars = {}
        self._funcs = {}
        self._explicit_binary_sign_switcher = {
            '/': self._division_manager,
            '%': self._division_manager,
            '=': self._assignation_manager,
            '/=': self._assignation_division_manager,
            '%=': self._assignation_division_manager,
            '+=': self._assignation_add_manager,
            '-=': self._assignation_sub_manager,
            '*=': self._assignation_mul_manager,
        }
        self._divider_switcher = {
            'Literal': self._divider_final_manager,
            'Id': self._divider_var_manager,
        }
        self._explicit_decl_switcher = {
            'FuncType': self._decl_func,
            'PrimaryType': self._decl_scalar,
        }
        self._binary_eval_switcher = {
            'Func': self._eval_func,
            'Binary': self._eval_binary,
            'Literal': self._eval_literal,
            'Id': self._eval_id,
        }
        self._binary_eval_expr_switcher = {
            '+': self._do_add,
            '-': self._do_sub,
            '/': self._do_div,
            '%': self._do_mod,
            '*': self._do_mul,
            '+=': self._do_iadd,
        }
        self._scope_switcher = {
            'Decl': self._named_scope,
            'While': self._lambda_loop_scope,
        }
        self._test_switcher = {
            'Binary': self._test_binary,
        }
        self._tester_switcher = {
            '<': self._test_le,
        }
        self._evaluator_switcher = {
            'Binary': self._eval_binary,
        }

    def _test_le(self, node, params, *args):
        lvalue = params[0]
        rvalue = params[1]
        lvalue_eval = self.bin_eval(lvalue)
        rvalue_eval = self.bin_eval(rvalue)
        l = lvalue_eval(lvalue)
        r = rvalue_eval(rvalue)
        self._logger.debug("{l} < {r} == {result}".format(l=l,
                                                          r=r,
                                                          result=int(l)<int(r)))
        return int(l) < int(r)

    def _test_binary(self, cond, *args, **kwargs):
        tester = self.tester(cond.call_expr)
        return tester(cond.call_expr, cond.params)

    def tester(self, node, *args):
        def trap(node, *args):
            self._logger.debug("Tester : {node}".format(node=node))
        return self._tester_switcher.get(node.value, trap)

    def scope(self, node):
        """
        retrieve the function which define if the whole scope is named or not
        """
        def trap(node, *args, **kwargs):
            self._logger.debug("scope : {node}"\
                               .format(node=node))
        return self._scope_switcher.get(type(node).__name__,
                                        trap)

    def _named_scope(self, node, **kwargs):
        """
        push the name of the named scope
        """
        self._scopes.append(node._name)
        self._logger.debug("named scope : {}".format(node._name))

    def _lambda_loop_scope(self, node, **kwargs):
        """
        generate a name for the anonymous loop scope and push it
        """
        self._scopes.append("loop_{id}".format(id=self.loop_id))
        self._logger.debug("lambda scope : loop_{id}".format(id=self.loop_id))
        self.loop_id += 1

    def _assignation_manager(self, node, **kwargs):
        bin_eval = self.bin_eval(node.params[1])
        if isinstance(node.params[0], Id):
            var = self.mangle_var(node.params[0].value)
            val = bin_eval(node.params[1])
            if val is not None:
                self._vars[var] = val

    def _assignation_division_manager(self, node, **kwargs):
        bin_eval = self.bin_eval(node.params[1])
        val = bin_eval(node.params[1])
        if isinstance(node.params[0], Id):
            var = self.mangle_var(node.params[0].value)
            if val is None or int(val) is 0:
                self._logger.info("/!\\ Division by zero '{var}' in {scope} =C> {c_code} /!\\"\
                                  .format(var=node.params[0].value,
                                          scope=" > ".join(self._scopes),
                                          c_code=node.to_c()))
                self._vars[var] = None
            else:
                if self._vars[var] is not None:
                    self._vars[var] /= int(val)
                else:
                    self._vars[var] = 0

    def _assignation_add_manager(self, node, **kwargs):
        bin_eval = self.bin_eval(node.params[1])
        val = bin_eval(node.params[1])
        scopes = self._scopes.copy()
        if isinstance(node.params[0], Id):
            var = None
            while len(scopes) >= 1:
                var = self.mangle_var(node.params[0].value, scopes)
                scopes.pop()
                if var in self.vars:
                    break
            if val is None:
                val = 0
            if self.vars[var] is None:
                self._vars[var] = 0
            self._vars[var] = int(self.vars[var]) + int(val)

    def _assignation_sub_manager(self, node, **kwargs):
        bin_eval = self.bin_eval(node.params[1])
        val = bin_eval(node.params[1])
        if isinstance(node.params[0], Id):
            var = self.mangle_var(node.params[0].value)
            if val is None:
                val = 0
            if self.vars[var] is None:
                self._vars[var] = 0
            self._vars[var] -= int(val)

    def _assignation_mul_manager(self, node, **kwargs):
        bin_eval = self.bin_eval(node.params[1])
        val = bin_eval(node.params[1])
        if isinstance(node.params[0], Id):
            var = self.mangle_var(node.params[0].value)
            if val is None:
                val = 0
            if self.vars[var] is None:
                self._vars[var] = 0
            self._vars[var] *= int(val)

    def _decl_func(self, func, **kwargs):
        self._funcs[func._name] = None

    def mangle_var(self, var_name=None, scopes=None):
        if scopes is None:
            return ">".join([">".join(self.scopes), var_name])
        return ">".join([">".join(scopes), var_name])

    def _decl_scalar(self, scalar, **kwargs):
        self._vars[self.mangle_var(scalar._name)] = None

    def _divider_final_manager(self, divider, **kwargs):
        if int(divider.value) is 0:
            self._logger.info("/!\\ Explicit division by zero at line  /!\\")

    def _divider_var_manager(self, divider, **kwargs):
        var = None
        scopes = self._scopes.copy()
        while len(scopes) >= 1:
            var = self.mangle_var(divider.value, scopes)
            scopes.pop()
            if var in self.vars:
                break
        if var not in self.vars:
            self._logger.info("/!\\ '{var}' in {scope} isn't defined /!\\"\
                              .format(var=divider.value,
                                      scope=" > ".join(self._scopes)))
            return
        self._logger.debug("[!] {var} = {val}".format(var=var,
                                                      val=self.vars[var]))
        if self.vars[var] is None:
            self._logger.info("/!\\ '{var}' in {scope} is corrupted /!\\"\
                              .format(var=divider.value,
                                      scope=" > ".join(self._scopes)))
            return
        elif int(self.vars[var]) is 0:
            self._logger.info("/!\\ Division by zero '{var}' in {scope} at line"\
                              .format(var=divider.value,
                                      scope=" > ".join(self._scopes))),
            return

    def _division_manager(self, node, **kwargs):
        evaluate_divider = self.evaluate_divider(node.params[1])
        evaluate_divider(node.params[1])

    def evaluate_divider(self, divider):
        def trap(divider, **kwargs):
            self._logger.debug("evaluate_divider : {divider} is '{type}'"\
                               .format(divider=divider,
                                       type=type(divider).__name__))
        return self._divider_switcher.get(type(divider).__name__, trap)

    def explicit_binary(self, node):
        def trap(node, **kwargs):
            self._logger.debug("explicit_binary : {node}"\
                               .format(node=node))
        return self._explicit_binary_sign_switcher.get(node.call_expr.value,
                                                       trap)

    def explicit_decl(self, node):
        def trap(node, **kwargs):
            self._logger.debug("explicit_decl : {node}"\
                               .format(node=node))
        return self._explicit_decl_switcher.get(type(node._ctype).__name__,
                                                trap)

    def bin_eval(self, param):
        """
        retrieve the evaluator of Binary node
        """
        def trap(param, *args, **kwargs):
            self._logger.debug("bin_eval : {param}"\
                               .format(param=param))
            return None
        return self._binary_eval_switcher.get(type(param).__name__,
                                              trap)

    def _eval_literal(self, literal):
        return literal.value

    def _eval_binary(self, node, **kwargs):
        eval_expr = self._eval_expr(node.call_expr.value)
        return eval_expr(node.params[0], node.params[1])

    def _eval_func(self, node, **kwargs):
        return 0

    def _eval_id(self, node, **kwargs):
        scopes = self._scopes.copy()
        var = None
        while len(scopes) >= 1:
            var = self.mangle_var(node.value, scopes)
            scopes.pop()
            if var in self.vars:
                break
        mangled_var = self.vars[var]
        return mangled_var if mangled_var is not None else 0

    def _do_add(self, a, b):
        """
        Perform addition
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        return int(lvalue) + int(rvalue)

    def _do_iadd(self, a, b):
        """
        Perform addition
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        var = None
        scopes = self._scopes.copy()
        while len(scopes) >= 1:
            var = self.mangle_var(a.value, scopes)
            scopes.pop()
            if var in self.vars:
                break
        self.vars[var] = int(lvalue) + int(rvalue)
        self._logger.debug(self.vars[var])
        return self.vars[var]

    def _do_sub(self, a, b):
        """
        Perform substraction
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        return int(lvalue) - int(rvalue)

    def _do_mul(self, a, b):
        """
        Perform multiplication
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        return int(lvalue) * int(rvalue)

    def _do_div(self, a, b):
        """
        Perform division
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        if int(rvalue) is 0:
            self._logger.info("/!\\ Explicit division by zero at line /!\\")
            return 0
        return int(lvalue) / int(rvalue)

    def _do_mod(self, a, b):
        """
        Perform modulo
        """
        a_bin_eval = self.bin_eval(a)
        b_bin_eval = self.bin_eval(b)
        lvalue = a_bin_eval(a)
        rvalue = b_bin_eval(b)
        if int(rvalue) is 0:
            self._logger.info("/!\\ Explicit modulo by zero /!\\")
            return 0
        return int(lvalue) % int(rvalue)

    def _eval_expr(self, param):
        """
        retrieve operator function in aim to evaluate the expression
        """
        def trap(param, *args, **kwargs):
            self._logger.debug("eval_expr : {param}"\
                               .format(param=param))
        return self._binary_eval_expr_switcher.get(param,
                                                   trap)

    def test(self, cond):
        def trap(cond, *args, **kwargs):
            self._logger.debug("test : {cond}"\
                               .format(cond=cond))
            return True
        return self._test_switcher.get(type(cond).__name__, trap)

    def evaluator(self, node):
        def trap(node):
            self._logger.debug("evaluator : {node}".format(node=node))
        return self._evaluator_switcher.get(type(node).__name__, trap)

    def _eval_body(self, node, *args):
        if type(node).__name__ is "ExprStmt":
            evaluator = self.evaluator(node.expr)
            evaluator(node.expr)
        else:
            pass

    @property
    def scopes(self):
        return self._scopes

    @property
    def vars(self):
        return self._vars

    @property
    def funcs(self):
        return self._funcs

    def _binary(self, node, parent, *args, **kwargs):
        super(DivBy0, self)._binary(node, parent, *args, **kwargs)
        binary = self.explicit_binary(node)
        if type(parent).__name__ is "Binary" and\
           parent.call_expr.value is '=':
            return
        binary(node, **kwargs)

    def _decl(self, node, parent, *args, **kwargs):
        super(DivBy0, self)._decl(node, parent, *args, **kwargs)
        decl = self.explicit_decl(node)
        decl(node, **kwargs)

    def _while(self, node, parent, *args, **kwargs):
        super(DivBy0, self)._while(node, parent, *args, **kwargs)
        while self._test(node.condition) is True:
            self._eval_body(node.body, parent)

    def _test(self, condition):
        test = self.test(condition)
        return test(condition)

    @Tracker(fortrack, backtrack)
    def switch_func(self, node, parent, *args, **kwargs):
        return super(DivBy0, self).switch_func(node, parent, *args, **kwargs)
