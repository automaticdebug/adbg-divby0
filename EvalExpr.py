class EvalExpr:
    def __init__(self , expr):
        self.opStack = []
        self.numStack = []
        self.expr = ""
        self.expr = self.formatExpr(expr)
        self.EvalExpr()
        print(self.expr)

    def EvalExpr(self):
        self.opStack.append(self.expr[1])
        while self.opStack:
            self.ProcessExpression(1)
        return self.numStack[0]

    def ProcessPriority(self, op):
        while self.definePriority(op) <= self.definePriority(self.opStack[0]):
            self.FastEval()

    def ProcessExpression(self, pos):
        if self.isOperator(self.expr[pos]):
            op = self.opFormat(pos)
            if self.expr[pos] == '(':
                self.opStack.append(self.expr[pos])
            elif self.expr[pos] == ')':
                self.EvalBracket()
            elif self.isUnary(op):
                self.opStack.append(self.expr[pos])
            elif self.definePriority(self.expr[pos] <=
                                     self.definePriority(self.opStack[-1])):
                self.processPriority(op)
                self.opStackAppend(op)
            else:
                self.opStack.append(op)
            pos += 1
        else:
            self.numStack.append(self.getIntFromExpr(pos))

    def FastEval(self):
        op = ''
        arg1 = 0
        arg2 = 0
        res = 0
        if self.opStack[-1] != '(':
            if self.isUnary(self.opStack[-1]):
                arg1 = self.numStack.pop()
                self.putIntOnStack(arg1)
            else:
                arg1 = self.numStack.pop()
                arg2 = self.numStack.pop()
                op = self.opStack.pop()
                res = self.evalBinary(arg1, arg2, op)
                self.putIntOnStack(res)

    def EvalBrackets(self):
        op = ''
        arg1 = 0
        arg2 = 0
        res = 0

        if self.opStack[-1] == '(':
            self.opStack.pop()
            arg1 = self.numStack.pop()
            self.putIntOnStack(arg1)
            pass
        arg1 = self.numStack.pop()
        arg2 = self.numStack.pop()
        op = self.opStack.pop()
        res = self.EvalBinary(arg1, arg2, op)
        self.putIntOnStack(res)
        if self.opStack[-1] != '(':
            self.FastEval()
        self.opStack.pop()
        if opStack:
            self.FastEval()

    def putIntOnStack(self, arg):
        c = ''
        if self.opStack and self.isUnary(self.opStack[-1]):
            c = self.opStack.pop()
            self.numStack.append(self.evalUnary(arg, c))
        else:
            self.numStack.append(arg)

    def EvalBinary(self, arg1, arg2, op):
        if op == '+':
            return arg1 + arg2
        if op == '-':
            return arg1 - arg2
        if op == '*':
            return arg1 * arg2
#besoin logger si arg2 == 0
        if op == '/':
            return arg1 / arg2
        if op == '%':
            return arg1 % arg2

    def EvalUnary(self, arg, c):
        return -arg if c == 'm' else arg

# Tools
    def formatExpr(self, expr):
        return "(" + expr + ")"

    def isOperator(self, c):
        return c in '-+*/%()'

    def isUnary(self, c):
        return c in 'pm'

    def definePriority(self, c):
        if c in '()':
            return 0
        if c in '-+':
            return 1
        if c in '/*%':
            return 2
        return -1

    def getIntFromExpr(self, pos):
        pass

    def opFormat(self, pos):
        if pos == 0 and self.expr[pos] == '-' or self.expr[pos] == '+':
            if self.is_operator(self.expr[pos-1])\
               and self.expr[pos-1] != ')':
                return 'm' if '-' else 'p'
        return self.expr[pos]

if __name__ == '__main__':
    e = EvalExpr("5+2")
