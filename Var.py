class Var(object):

    def __init__(self, type, value):
        self._type = type
        self._value = value

    @property
    def value(self):
        return self._value

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, type):
        self._type = type

    @value.setter
    def value(self, value):
        self._value = value

    def __add__(self, rvalue):
        if self.type is rvalue.type:
            return self.value + rvalue.value
        else:
            raise NotImplementedError("Cannot add")

    def __sub__(self, rvalue):
        if self.type is rvalue.type:
            return self.value - rvalue.value
        else:
            raise NotImplementedError("Cannot sub")

    def __mul__(self, rvalue):
        if self.type is rvalue.type:
            return self.value * rvalue.value
        else:
            raise NotImplementedError("Cannot mul")

    def __div__(self, rvalue):
        if self.type is rvalue.type:
            return self.value / rvalue.value
        else:
            raise NotImplementedError("Cannot div")

    def __mod__(self, rvalue):
        if self.type is rvalue.type:
            return self.value % rvalue.value
        else:
            raise NotImplementedError("Cannot mod")

    def __iadd__(self, rvalue):
        if self.type is rvalue.type:
            return self.value + rvalue.value
        else:
            raise NotImplementedError("Cannot add")

    def __isub__(self, rvalue):
        if self.type is rvalue.type:
            return self.value - rvalue.value
        else:
            raise NotImplementedError("Cannot sub")

    def __imul__(self, rvalue):
        if self.type is rvalue.type:
            return self.value * rvalue.value
        else:
            raise NotImplementedError("Cannot mul")

    def __idiv__(self, rvalue):
        if self.type is rvalue.type:
            return self.value / rvalue.value
        else:
            raise NotImplementedError("Cannot div")

    def __imod__(self, rvalue):
        if self.type is rvalue.type:
            return self.value % rvalue.value
        else:
            raise NotImplementedError("Cannot mod")
